<?php

namespace App\Entity\VirtueMart;

use App\Repository\VirtueMart\StateRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=StateRepository::class)
 * @ORM\Table(name="states")
 */
class State
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=UserInfo::class, mappedBy="state")
     */
    private $userInfoId;

    public function __construct()
    {
        $this->userInfoId = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|UserInfo[]
     */
    public function getUserInfoId(): Collection
    {
        return $this->userInfoId;
    }

    public function addUserInfoId(UserInfo $userInfoId): self
    {
        if (!$this->userInfoId->contains($userInfoId)) {
            $this->userInfoId[] = $userInfoId;
            $userInfoId->setState($this);
        }

        return $this;
    }

    public function removeUserInfoId(UserInfo $userInfoId): self
    {
        if ($this->userInfoId->removeElement($userInfoId)) {
            // set the owning side to null (unless already changed)
            if ($userInfoId->getState() === $this) {
                $userInfoId->setState(null);
            }
        }

        return $this;
    }
}
