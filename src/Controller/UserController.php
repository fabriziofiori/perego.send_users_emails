<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use App\Service\Email\UserEmailFactory;
use App\Service\Email\UserEmailService;
use App\Entity\User;

class UserController extends BaseController
{
    /**
     * @return Response
     */
    #[Route('/users', name: 'user_index')]
    public function index(): Response
    {
        return $this->render('user/index.html.twig', [
            'users' => $this->getDoctrine()->getRepository(User::class)->findAll(),
        ]);
    }

    /**
     * @param UserEmailService $userEmailService
     * @param string $emailType
     * @return Response
     */
    #[Route('user/send-email/{emailType}', name: 'user_send_email')]
    public function sendEmail(UserEmailService $userEmailService, string $emailType): Response
    {
        $userEmailFactory = new UserEmailFactory($emailType);

        $users = $this->getDoctrine()
            ->getRepository(User::class)
            ->findAll();

        foreach($users as $user)
        {
            $userEmail = $userEmailFactory->createInstance($user);
            $userEmail->from('fabrizio.fiori@dijiti.com');

            $userEmailService->send($userEmail);
        }
    }
}
