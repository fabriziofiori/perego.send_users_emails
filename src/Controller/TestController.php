<?php

namespace App\Controller;

use App\Entity\User;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\ORM\Mapping\Column;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

class TestController extends BaseController
{
    /**
     * @param int $min
     * @param int $max
     * @return Response
     */
    #[Route('/test/action/{min}/{max}', name: 'test_action', defaults: ['min' => 0,'max' => 100])]
    public function action(int $min = 0, int $max = 0): Response
    {
        $number = random_int($min, $max);

        return $this->render('test/action.html.twig', [
            'number' => $number,
        ]);
    }

    #[Route('/test/reflection', name: 'test_reflection')]
    public function reflection(): Response
    {
        $annotationReader = new AnnotationReader();
        $refClass = new \ReflectionClass(User::class);

        foreach ($refClass->getProperties() as $refProperty)
        {
            echo($refProperty->name);echo(" => ");

            $propertyAnnotations = $annotationReader->getPropertyAnnotations($refProperty);
            foreach($propertyAnnotations as $propertyAnnotation)
            {
                if($propertyAnnotation instanceof Column)
                {
                    var_dump($propertyAnnotation->type);echo("<br>");
                }
            }
        }

        die;
    }
}