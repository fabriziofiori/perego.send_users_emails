<?php

namespace App\Service\Email;

use Symfony\Component\Mailer\MailerInterface;

class UserEmailService
{
    /**
     * @var MailerInterface
     */
    private MailerInterface $mailer;

    function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * @param AbstractUserEmail $userEmail
     */
    public function send(AbstractUserEmail $userEmail): void
    {
        $this->mailer->send($userEmail);
    }
}