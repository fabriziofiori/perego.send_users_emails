<?php

namespace App\Service\Email;

use ReflectionClass;

class UserEmailFactory
{
    private string $emailTypeClassFullName;
    private ?ReflectionClass $reflectionClass;

    function __construct(string $emailType)
    {
        $this->emailTypeClassFullName = "App\Service\Email\User".ucfirst($emailType)."Email";
        $this->reflectionClass = null;
    }

    private function getReflectionClass(): ReflectionClass
    {
        if($this->reflectionClass === null)
        {
            try
            {
                $this->reflectionClass = new ReflectionClass($this->emailTypeClassFullName);
            }
            catch(ReflectionException $ex)
            {
                $this->createNotFoundException();
            }
        }

        return $this->reflectionClass;
    }

    public function createInstance(array|object $arg = [])
    {
        if(!is_array($arg))
        {
            $arg = [$arg];
        }

        return $this->getReflectionClass()->newInstanceArgs($arg);
    }
}