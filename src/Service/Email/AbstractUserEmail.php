<?php

namespace App\Service\Email;

use App\Entity\User;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;

class AbstractUserEmail extends TemplatedEmail
{
    /**
     * @var User
     */
    private User $user;

    function __construct(User $user)
    {
        parent::__construct();

        $this->user = $user;

        $this->to($user->getEmail());
    }
}