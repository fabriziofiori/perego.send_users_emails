<?php

namespace App\Service\Email;

use App\Entity\User;

class UserImportEmail extends AbstractUserEmail
{
    function __construct(User $user)
    {
        parent::__construct($user);

        $this->subject('Profile migration notification')
            ->htmlTemplate('emails/user_import.html.twig')
            ->context([
                'user' => $user
            ]);
    }
}